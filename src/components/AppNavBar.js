import { Fragment, useContext } from "react";
import { Container, Nav, Navbar } from "react-bootstrap";
import { Link } from "react-router-dom";
import UserContext from "../UserContext";


export default function AppNavbar() {

    // const { user, setUser } = useContext(UserContext);

    const { user } = useContext(UserContext);

    return (
    <Navbar bg="primary" expand="lg">
    <Container >
        <Navbar.Brand as={Link} to="/">Masining PH</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
            <Nav
                className="col d-flex justify-content-end me-auto my-2 my-lg-0"
                style={{ maxHeight: '100%' }}
                
                >
                <Nav.Link as={Link} to="/" >Home</Nav.Link>
                <Nav.Link as={Link} to="/products" >Products</Nav.Link>
                {(user.id !== null) ?
                <Fragment>

                <Nav.Link as={Link} to="/logout" >Logout</Nav.Link>
                </Fragment>
                :
                <Fragment>
                <Nav.Link as={Link} to="/register" >Register</Nav.Link>
                <Nav.Link disabled>|</Nav.Link>
                <Nav.Link as={Link} to="/login" >Login</Nav.Link>
                </Fragment>
                }
            </Nav>
        </Navbar.Collapse>
    </Container>
  </Navbar>
    )
}




