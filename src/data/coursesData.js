import img1 from '../images/PHP.jpg';
import img2 from '../images/Python.jpg';
import img3 from '../images/Java.jpg';
const data = {
    coursesData: [
    {
        id: "wdc001",
        img: img1,
        name: "PHP-Laravel",
        description: "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Mollitia, aliquid? ",
        price: 45000,
        category: "necklace",
        onOffer: true
    },
    {
        id: "wdc002",
        img: img2,
        name: "Python - Django",
        description: "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Mollitia, aliquid?",
        price: 50000,
        category: "necklace",
        onOffer: true
    },
    {
        id: "wdc003",
        img: img3,
        name: "JAVA - Springboot",
        description: "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Mollitia, aliquid?",
        price: 55000,
        category: "necklace",
        onOffer: true
    },
    {
        id: "wdc001",
        img: img1,
        name: "PHP-Laravel",
        description: "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Mollitia, aliquid? ",
        price: 45000,
        category: "necklace",
        onOffer: true
    },
    {
        id: "wdc002",
        img: img2,
        name: "Python - Django",
        description: "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Mollitia, aliquid?",
        price: 50000,
        category: "necklace",
        onOffer: true
    },
    {
        id: "wdc003",
        img: img3,
        name: "JAVA - Springboot",
        description: "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Mollitia, aliquid?",
        price: 55000,
        category: "necklace",
        onOffer: true
    }
]}

export default data;
