import { Row, Col, Button } from 'react-bootstrap';



export default function Home() {


  return(

    <Row className="container mx-auto my-3 py-3" id="home-box">
      <Col className="col-7 mx-auto">
        <img src="Add-to-Cart.gif" className="img-fluid" controlId="shopper" />
      </Col>
      <Col className="my-auto">
        <Row>
          <Col className="pl-4">
            <h1 className="text-center">We are Now Open!</h1>
          </Col>
        </Row>
        <Row>
          <Col className="position-relative">
            <img src="welcome.jpeg" className="img-fluid d-none d-sm-block" />
          </Col>
        </Row>
      </Col>
    </Row>



    );
};
