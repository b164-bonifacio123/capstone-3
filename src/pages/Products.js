import { Container, Form, InputGroup, Dropdown, DropdownButton, FormControl, CardColumns, Card, Button } from 'react-bootstrap';



export default function() {


    return(

        <Container className="mt-2 py-3 bg-light">
{/*            <InputGroup className="mb-3">
              <DropdownButton
                as={InputGroup.Prepend}
                variant="outline-secondary"
                title="Search"
                id="input-group-dropdown-1"
              >
                <Dropdown.Item href="#">Mobile & accessories</Dropdown.Item>
                <Dropdown.Item href="#">Clothes</Dropdown.Item>
                <Dropdown.Item href="#">Kitchenwares</Dropdown.Item>
                <Dropdown.Divider />
                <Dropdown.Item href="#">All Products</Dropdown.Item>
              </DropdownButton>
              <FormControl aria-describedby="basic-addon1" />
            </InputGroup>*/}

            <CardColumns className="my-5 py-5">
                <Card style={{ width: '18rem' }}>
                  <Card.Img variant="top" src="jeepney-bag.jpeg" />
                  <Card.Body>
                    <Card.Title>Jeepney Bag</Card.Title>
                    <Card.Subtitle>Description:</Card.Subtitle>
                    <Card.Text>
                      Ethical handcrafted bags by local Filipino artists
                    </Card.Text>
                    <Card.Subtitle>Price:</Card.Subtitle>
                    <Card.Text>P1,000</Card.Text>
                    <div className="d-flex justify-content-around">
                        <Button variant="primary">Add to Cart</Button>
                        <Button variant="warning">Check Details</Button>
                    </div>
                  </Card.Body>
                </Card>

                <Card style={{ width: '18rem' }}>
                  <Card.Img variant="top" src="kalabaw-bag.jpeg" />
                  <Card.Body>
                    <Card.Title>Kalabaw Bag</Card.Title>
                    <Card.Subtitle>Description:</Card.Subtitle>
                    <Card.Text>
                      Ethical handcrafted bags by local Filipino artists
                    </Card.Text>
                    <Card.Subtitle>Price:</Card.Subtitle>
                    <Card.Text>P1,000</Card.Text>
                    <div className="d-flex justify-content-around">
                        <Button variant="primary">Add to Cart</Button>
                        <Button variant="warning">Check Details</Button>
                    </div>
                  </Card.Body>
                </Card>

                <Card style={{ width: '18rem' }}>
                  <Card.Img variant="top" src="sorbetero-bag.jpeg" />
                  <Card.Body>
                    <Card.Title>Sorbetero Bag</Card.Title>
                    <Card.Subtitle>Description:</Card.Subtitle>
                    <Card.Text>
                      Ethical handcrafted bags by local Filipino artists
                    </Card.Text>
                    <Card.Subtitle>Price:</Card.Subtitle>
                    <Card.Text>P1,000</Card.Text>
                    <div className="d-flex justify-content-around">
                        <Button variant="primary">Add to Cart</Button>
                        <Button variant="warning">Check Details</Button>
                    </div>
                  </Card.Body>
                </Card>

                <Card style={{ width: '18rem' }}>
                  <Card.Img variant="top" src="ligo-bag.jpeg" />
                  <Card.Body>
                    <Card.Title>Ligo Bag</Card.Title>
                    <Card.Subtitle>Description:</Card.Subtitle>
                    <Card.Text>
                      Ethical handcrafted bags by local Filipino artists
                    </Card.Text>
                    <Card.Subtitle>Price:</Card.Subtitle>
                    <Card.Text>P1,000</Card.Text>
                    <div className="d-flex justify-content-around">
                        <Button variant="primary">Add to Cart</Button>
                        <Button variant="warning">Check Details</Button>
                    </div>
                  </Card.Body>
                </Card>

                <Card style={{ width: '18rem' }}>
                  <Card.Img variant="top" src="radyo-bag.jpeg" />
                  <Card.Body>
                    <Card.Title>Radyo Bag</Card.Title>
                    <Card.Subtitle>Description:</Card.Subtitle>
                    <Card.Text>
                      Ethical handcrafted bags by local Filipino artists
                    </Card.Text>
                    <Card.Subtitle>Price:</Card.Subtitle>
                    <Card.Text>P1,000</Card.Text>
                    <div className="d-flex justify-content-around">
                        <Button variant="primary">Add to Cart</Button>
                        <Button variant="warning">Check Details</Button>
                    </div>
                  </Card.Body>
                </Card>

                <Card style={{ width: '18rem' }}>
                  <Card.Img variant="top" src="padyak-bag.jpg" />
                  <Card.Body>
                    <Card.Title>Padyak Bag</Card.Title>
                    <Card.Subtitle>Description:</Card.Subtitle>
                    <Card.Text>
                      Ethical handcrafted bags by local Filipino artists
                    </Card.Text>
                    <Card.Subtitle>Price:</Card.Subtitle>
                    <Card.Text>P1,000</Card.Text>
                    <div className="d-flex justify-content-around">
                        <Button variant="primary">Add to Cart</Button>
                        <Button variant="warning">Check Details</Button>
                    </div>
                  </Card.Body>
                </Card>

            </CardColumns>


        </Container>


        )
}
