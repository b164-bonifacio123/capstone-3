import { Fragment, useContext, useEffect, useState } from "react";
import AppNavbar from "../components/AppNavbar";
import UserContext from "../UserContext";
import { Container, Row, Col, Button, Card, Nav } from "react-bootstrap";
import { Link, useParams } from "react-router-dom";

export default function ProductView() {

    const { user } = useContext(UserContext);

    const { productId } = useParams();

    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState(0);
    const [stock, setStock] = useState(0);

    useEffect(() => {
        console.log(productId)
        fetch(`https://${productId}`)
        .then(res => res.json())
        .then(data => {
            console.log(data)

            setName(data.name)
            setDescription(data.description)
            setPrice(data.price)
            setStock(data.stock)
        })
    })

  return (
    <Fragment>
        <AppNavbar/>
        <Nav.Link as={Link} to='/products'>Back</Nav.Link>
        <Container className="mt-5">
				<Row>
					<Col lg={{span:6, offset:3}}>
						<Card>
							<Card.Body className="text-center">
								<Card.Title>{name}</Card.Title>
								<Card.Text>Description: {description}</Card.Text>
								<Card.Text>Php {price}</Card.Text>
								<Card.Subtitle>Stocks:</Card.Subtitle>
								<Card.Text>{stock}</Card.Text>
						{
							user.id !== null ?
								<Button variant="primary">Add to cart</Button>
								:
							<Link className="btn btn-danger" to="/login">Log in</Link>

						}

							</Card.Body>
						</Card>
					</Col>
				</Row>
			</Container>

    </Fragment>
    
  )
}
