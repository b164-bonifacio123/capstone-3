import { Fragment } from "react";
import { Row, Col, Button } from "react-bootstrap";
import { Link } from "react-router-dom";
// import AppNavbar from "../components/AppNavbar";

export default function ErrorPage() {
    
    return(
        <Fragment>
{/*            <AppNavbar/>*/}
            <Row className="mt-5">
                <Col>
                    <h1 className="text-danger">404 Page not found</h1>
                <Button as={Link} to="/">Home</Button>
                </Col>
            </Row>
        </Fragment>
    )
};

